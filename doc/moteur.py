#Import des bibliothèques nécessaires au fonctionnement du programme... 
#Cet import n'est pas nécessaire dans le cas du serveur car elles sont déjà importées dans le fichier principal.
import pygame
import RPi.GPIO as GPIO

# Initialisation du servo moteur
servoPIN = 18 # Numéro de la broche GPIO utilisée pour le servo
GPIO.setmode(GPIO.BCM) # Utilisation de la numérotation de la carte
GPIO.setup(servoPIN, GPIO.OUT) # Définition de la broche GPIO comme sortie
p = GPIO.PWM(servoPIN, 50)  # PWM à 50Hz
p.start(0)  # Initialization

pygame.init() # Initialisation de Pygame

# Déterminez le nombre de manettes connectées
nombre_manettes = pygame.joystick.get_count() # Retourne le nombre de manettes connectées

try: # Permet de gérer les erreurs
    if nombre_manettes == 0: # Si aucune manette n'est détectée
        print("Aucune manette détectée.") # Affiche un message d'erreur
    else: # Sinon
        # Initialisez toutes les manettes détectées
        for i in range(nombre_manettes): # Pour chaque manette détectée
            manette = pygame.joystick.Joystick(i) # Initialise la manette
            manette.init() # Active la manette
            print("Manette {} détectée: {}".format(i+1, manette.get_name())) # Affiche un message de confirmation

        while True: # Boucle infinie
            for event in pygame.event.get(): # Pour chaque événement détecté
                if event.type == pygame.JOYBUTTONDOWN and event.button == 4: # Si le bouton 4 est pressé
                    p.ChangeDutyCycle(7.15) # Change le rapport cyclique à 7.15%
                elif event.type == pygame.JOYBUTTONDOWN and event.button == 5: # Si le bouton 5 est pressé
                    p.ChangeDutyCycle(8) # Change le rapport cyclique à 8%
                elif event.type == pygame.JOYBUTTONUP and event.button == 4: # Si le bouton 4 est relâché
                    p.ChangeDutyCycle(0) # Change le rapport cyclique à 0%
                elif event.type == pygame.JOYBUTTONUP and event.button == 5: # Si le bouton 5 est relâché
                    p.ChangeDutyCycle(0) # Change le rapport cyclique à 0%

except KeyboardInterrupt: # Si l'utilisateur appuie sur CTRL+C
    p.stop() # Arrête le servo
    GPIO.cleanup() # Réinitialise les ports GPIO
