import eteint
import config
from libs_externes import *


class Case:
    """Classe pour une case du jeu : un mur ou un chemin.

    Attributs:
    ----------
    - `x`, `y` : int, int
        - Les coordonnées de la case.
    - `visitee` : bool
        - Si la case à été visitée lors de la génération du labyrinthe.
    - `valeur` : int
        - La valeur de la case : 1, un mur, 0, un chemin.

    Méthodes:
    ---------
    - ``coords(x: int, y: int)`` -> list[int,int]
        - Obtenir les coordonnées de la case, sous la forme [x,y].
    - ``set_visitee()`` -> None
        - Fixe la case comme visitée.
    - ``est_chemin()`` -> None
        - Fixe la case comme étant un chemin.
    - ``get_visitee()`` -> bool
        - Savoir si la case a déja été visitée.
    """
    DIR = [(-2, 0), (2, 0), (0, -2), (0, 2)]

    def __init__(self, x, y) -> None:
        # Cf. 'Case.__doc__' pour la définition des attributs.
        self.x = x
        self.y = y
        self.visitee = False
        self.valeur = 1  # Valeur de mur.

    def coords(self) -> list[int, int]:
        return [self.x, self.y]

    def set_visitee(self) -> None:
        self.visitee = True

    def est_chemin(self) -> None:
        self.valeur = 0

    def get_visitee(self) -> bool:
        return self.visitee


class Joueur:
    """Classe pour le joueur du jeu.

    Attributs:
    ----------
    - `x`, `y` : int, int
        - Les coordonnées de la case.
    - `direction` : int
        - La direction du joueur : 
            - -1, aucune direction
            - 0, bas; 1, droite; 2, haut; 3, gauche.

    Méthodes:
    ---------
    - ``coords(x: int, y: int)`` -> list[int,int]
        - Obtenir les coordonnées de la case, sous la forme [x,y].
    - ``deplacement(valeurs_voisins:dict, height:int)`` -> None
        - Déplacer le joueur.

    """

    def __init__(self, x, y, jeu) -> None:
        # Cf. 'Joueur.__doc__' pour la définition des attributs.
        self.x, self.y = x, y
        self.direction = -1
        self.jeu = jeu

    def coords(self) -> list[int, int]:
        return [self.x, self.y]

    def deplacement(self, valeurs_voisins: dict, height: int) -> None:
        """Modifie les coordonnées du personnage.

        Args:
            valeurs_voisins (dict): Le dictionnaire contant la valeur des voisins du joueur.
            height (int): La hauteur du terrain
        """
        if self.jeu.SUR_LEDS:
            self.jeu.afficherSurLed(self.x, self.y, color=None)

        if self.direction == 0:
            if valeurs_voisins[0] != 1:  # BAS
                self.y = (self.y-1) % (height)
            else:
                self.direction = -1

        elif self.direction == 2:
            if valeurs_voisins[2] != 1:  # HAUT
                self.y = (self.y+1) % (height)
            else:
                self.direction = -1

        elif self.direction == 1:
            if valeurs_voisins[1] != 1:  # DROITE
                self.x += 1
            else:
                self.direction = -1

        elif self.direction == 3:
            if valeurs_voisins[3] != 1:  # GAUCHE
                self.x -= 1
            else:
                self.direction = -1


class Labyrinthe:
    """Jeu de labyrinthe, descente au enfers non incluse. Vous n'êtes pas Thésée, que je sache.
    Si toutefois vous l'êtes, veuillez me contacter.

    Attributs:
    ----------
    - `width` : int
        - Largeur du panneau de LEDs.
    - `height` : int 
        - Hauteur du panneau de LEDs.
    - `SUR_LEDS` : bool
        - Si le jeu est lancé avec les LEDs.
    - `jeu` : bool
        - L'état de la partie : en cours (True), finie (False).
    - `fin` : list[int, int]
        - Les coordonnées de la case de fin (init: liste vide.)
    - `grille` : list[list[Case]]
        - Terrain modélisé par une matrice contenant toutes les cases.

    #### Si les LEDs sont branchées :
        - `strand` : NeoPixel_raspberry 
            - Séquence de neopixels, soit l'ensemble des LEDs du panneau.
        - `num_led` : int
            - Nombre de LEDs du panneau.
        - `manette` : pygame.joystick.Joystick
            - La manette pour contrôler le joueur.
    #### Sinon :
        - `listener` : keyboard.Listener
            - Instance pour récupérer les entrées clavier.
        - `pseudo_leds` : Pseudo_leds
            - Instance pour simuler les LEDs par un jeu pygame.

    Méthodes:
    ---------
    - ``adresseLED(x: int, y: int)`` -> int
        - Obtenir le numéro de la led correspondant aux coordonnées (x,y).
    - ``afficherSurLed(x: int, y: int, color)`` -> None
        - Afficher un pixel sur les LEDs avec la couleur donnée.
    - ``key_press_clavier(key: keyboard.Key)`` -> None
        - Gérer les entrées clavier pour diriger les snakes.
    - ``key_press_manette()`` -> None:
        - Gère les entrées manette pour diriger les Snakes.
    - ``generer()`` -> None
        - Génération d'un labyrinthe.
    - ``grille_valeurs()`` -> list[list[int]]
        - Obtenir la matrice contenant les valeurs de chaque case.
    - ``set_cases_vides()`` -> None
        - Produire une grille où toute case vide est entourée de murs.
    - ``voisins_case(case:Case)`` -> list[Case]
        - Obtenir la liste des cases vides voisines 
        (séparées d'un mur, qui sera potentiellement retiré pour créer un chemin) d'une case donnée.
    - ``briser_mur(case_courante:Case, voisin_choisi:Case)`` -> None
        - Retirer le mur qui sépare deux cases données, nécessairement voisines.
    - ``choisir_case_finale()`` -> list[int, int]
        - Choix d'une case de fin, parmi les cases de bout de chemins.
    - ``voisins_joueur()`` -> dict[int, int]
        - Obtenir les valeurs des voisins du joueur.
    - ``joueur_atteint_fin()`` -> None
        - Vérification de la victoire du joueur.
    - ``case_est_mur(x:int,y:int)`` -> bool
        - La case de coordonnées x,y est-elle un mur... ?
    - ``get_murs()`` -> list[int,int]
        - Obtenir la liste des coordonnées des murs.
    - ``dessiner()`` -> None
        - Afficher le jeu avec ou sans LEDs.
    - ``initMoteur()`` -> None
        - Initialisation du moteur, pour fair tourner le cylindre.
    - ``quitter()`` -> None
        - Quitter le jeu.        
    - ``mainloop()`` -> None
        - Code principal du jeu.
    """

    def __init__(self) -> None:
        self.width, self.height = config.WIDTH, config.HEIGHT
        self.SUR_LEDS = config.SUR_LEDS
        self.jeu = True
        self.fin = []

        # Créer une grille carrée de cellules...
        self.grille = [[Case(x, y) for x in range(self.width)]
                       for y in range(self.height)]
        # ... avec des murs entre elles !
        self.set_cases_vides()

        # Avec LEDs, initialisation de la manette, des LEDs, du moteur.
        if self.SUR_LEDS:

            # Manette
            pygame.init()
            self.nombre_manettes = pygame.joystick.get_count()
            assert self.nombre_manettes >= 1, f"Il n'y a pas de manette connectée !"

            self.manette = pygame.joystick.Joystick(0)
            self.manette.init()

            # Leds
            from config import strand, NUM_LED
            self.strand, self.num_led = strand, NUM_LED
            self.strand.show()

            # Moteur
            self.initMoteur()

        else:
            # Sans les LEDs, on joue à l'aide d'un clavier et d'une simulation pygame.
            from pynput import keyboard
            # Listener pour les entrées clavier.
            self.listener = keyboard.Listener(on_press=self.key_press_clavier)
            self.listener.start()

            # Affichage pygame
            from lib.Pseudo_affichage import Pseudo_leds
            self.pseudo_leds = Pseudo_leds([self.width-1, self.height-1])

        # Générer le labyrinthe.
        self.generer()

        # Sélectionner une case finale.
        self.fin = self.choisir_case_finale()

        # Mettre à jour l'affichage.
        self.dessiner(generation=True)

    def adresseLED(self, x: int, y: int) -> int:
        """Obtenir le numéro de la LED de coordonnées (x,y).

        Args:
            x (int): Abscisse de la led.
            y (int): Ordonné de la led.

        Returns:
            int: Numéro de la LED sur le panneau.
        """
        if y % 2 == 0:
            return ((y*config.WIDTH)+x)
        else:
            return ((y*config.WIDTH)+(config.WIDTH-1-x))

    def afficherSurLed(self, x: int, y: int, color=None) -> None:
        """Afficher un pixel sur les LEDs avec la couleur donnée."""
        if color == "PERSO": self.strand.setPixelColor(self.adresseLED(x, y), 0, 0, 255)
        elif color == "FIN": self.strand.setPixelColor(self.adresseLED(x, y), 255, 0, 0)
        elif color == "MUR": self.strand.setPixelColor(self.adresseLED(x, y), 0, 255, 0)
        else: self.strand.setPixelColor(self.adresseLED(x, y), 0, 0, 0)

    def key_press_clavier(self, key):
        """:param key: (z,s,d,q) Touches pour diriger le perso."""
        try :
            if key.char == 's': self.joueur.direction = 0
            elif key.char == 'd': self.joueur.direction = 1
            elif key.char == 'z': self.joueur.direction = 2
            elif key.char == 'q': self.joueur.direction = 3
            elif key.char == 'p': self.Jeu = False
        except:
            pass

    def key_press_manette(self) -> None:
        """Gérer les entrées manette pour diriger le snake."""
        for event in pygame.event.get():
            if event.type == pygame.JOYAXISMOTION:
                if event.joy == 0:  # manette 0
                    if event.axis == 0:
                        if self.manette.get_axis(event.axis) < -0.5:
                            self.joueur.direction = 1  # GAUCHE
                        elif self.manette.get_axis(event.axis) > 0.5:
                            self.joueur.direction = 3  # DROITE
                    elif event.axis == 1:
                        if self.manette.get_axis(event.axis) < -0.5:
                            self.joueur.direction = 2  # HAUT
                        elif self.manette.get_axis(event.axis) > 0.5:
                            self.joueur.direction = 0  # BAS

            if event.type == pygame.JOYBUTTONDOWN and event.button == 4:
                self.p.ChangeDutyCycle(7)
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 5:
                self.p.ChangeDutyCycle(8.2)
            elif event.type == pygame.JOYBUTTONUP and event.button == 4:
                self.p.ChangeDutyCycle(0)
            elif event.type == pygame.JOYBUTTONUP and event.button == 5:
                self.p.ChangeDutyCycle(0)

            # Stopper le jeu.
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 1:
                self.jeu = False

    def generer(self):
        # 2. Choisir une cellule de départ aléatoire dans la grille.
        dx, dy = random.randrange(
            1, self.width-1, 2), random.randrange(1, self.height-1, 2)
        case_depart = self.grille[dy][dx]
        self.joueur = Joueur(dx, dy, self)
        # 3. Marquer cette cellule comme visitée et l'ajouter à une pile.
        case_depart.set_visitee()
        case_depart.est_chemin()

        pile = [case_depart]
        # 4. Tant que la pile n'est pas vide :
        while pile != []:
            # a. Prendre la cellule au sommet de la pile et chercher ses voisins non visités.

            # self.dessiner()

            case_courante = pile[-1]
            voisins_non_visites = self.voisins_case(case_courante)

            # b. S'il y a au moins un voisin non visité, en choisir un au hasard.
            if voisins_non_visites != []:
                voisin_choisi = random.choice(voisins_non_visites)

                # c. Briser le mur entre la cellule en cours et la cellule choisie.
                self.briser_mur(case_courante, voisin_choisi)

                # d. Marquer la cellule choisie comme visitée et l'ajouter à la pile.
                voisin_choisi.set_visitee()
                voisin_choisi.est_chemin()
                pile.append(voisin_choisi)

            # e. Si la cellule en cours n'a pas de voisins non visités, la retirer de la pile.
            else:
                pile.pop()

    def grille_valeurs(self) -> list[list[int]]:
        return [[case.valeur for case in ligne] for ligne in self.grille]

    def set_cases_vides(self) -> None:
        for ligne in range(self.height):
            for colonne in range(self.width-1):
                if ligne % 2 == 1 and colonne % 2 == 1:
                    self.grille[ligne][colonne].est_chemin()
                else:
                    self.grille[ligne][colonne].visitee = True

    def voisins_case(self, case: Case) -> list[Case]:
        voisins = []
        for x1, y1 in Case.DIR:
            x_voisin, y_voisin = case.x+x1, (case.y+y1) % self.height
            if 0 <= x_voisin < self.width-1:
                voisin = self.grille[y_voisin][x_voisin]
                if voisin.get_visitee() == False:
                    voisins.append(voisin)
        return voisins

    def briser_mur(self, case_courante: Case, voisin_choisi: Case) -> None:
        x1, y1 = case_courante.coords()
        x2, y2 = voisin_choisi.coords()

        # S'il s'agit d'une paire de cases sur la limite du terrain.
        if y1-y2 not in [-2, 0, 2]:
            x, y = (x1+x2)//2, 0  # HARDCODED TODO en mieux.
        else:
            x, y = (x1+x2)//2, (y1+y2)//2

        # Coordonnées du mur à enlever :
        self.grille[y][x].est_chemin()

    def choisir_case_finale(self) -> list[int, int]:
        """Choix d'une case de fin, parmi les cases de bout de chemins.

        Returns:
            list[int,int]: La case retenue.
        """

        # On regardera les cases adjacentes directes.
        DIR1 = [(1, 0), (-1, 0), (0, 1), (0, -1)]

        # Init: Les cases de fin retenues.
        fins_possibles = []

        # Parcours de chaque case de la grille.
        for ligne in range(self.height):
            for colonne in range(self.width):
                case = self.grille[ligne][colonne]

                # Si la case est vide et qu'elle n'est pas la case de départ,
                if case.valeur == 0 and case.coords() != self.joueur.coords():

                    # Pour chacune de ses 4 cases adjacentes directes,
                    voisins = []
                    for x1, y1 in DIR1:
                        # On retient les coordonnées de cette nouvelle case
                        x, y = case.x+x1, (case.y+y1) % self.height
                        # Si elle est dans le terrain, et que ce n'est pas un mur (<=> valeur != 1),
                        # on la retient.
                        if 0 <= x < self.width:
                            voisin = self.grille[y][x]
                            if voisin.valeur != 1:
                                voisins.append(voisin)

                    # Après parcours de chaque case adjacente, si la case originelle n'a qu'un voisin,
                    # alors elle est au bout d'un chemin étant entourée de trois murs (ou d'un bord de terrain),
                    # On la retient comme fin potentielle.
                    if len(voisins) == 1:
                        fins_possibles.append(case)

        # Renvoyer les coordonnées d'une fin parmi toutes les fins possibles.
        return random.choice([c.coords() for c in fins_possibles])

    def voisins_joueur(self) -> dict[int, int]:
        """Obtenir les valeurs des voisins du joueur.
        On a : 0, bas; 1, droite; 2, haut; 3, gauche.
        Note: pour la gauche et la droite, si la case n'est pas 
        dans le terrain on renvoie 1 (<=> mur).

        Returns:
            dict: Le dictionnaire contant la valeur des voisins du joueur.
        """
        x, y = self.joueur.coords()
        return {
            0: self.grille[(y-1) % (self.height)][x].valeur,
            2: self.grille[(y+1) % (self.height)][x].valeur,
            1: self.grille[y][x+1].valeur if x+1 < self.width else 1,
            3: self.grille[y][x-1].valeur if x-1 >= 0 else 1
        }

    def joueur_atteint_fin(self):
        """Vérification de la victoire du joueur.
        """
        if self.joueur.coords() == self.fin:
            self.jeu = False

    def case_est_mur(self, x: int, y: int) -> bool:
        """La case de coordonnées x,y est-elle un mur... ?

        Args:
            x (int), y (int): coordonnées de la case.

        Returns:
            bool: Si la case est un mur.
        """
        return self.grille[y][x].valeur == 1

    def get_murs(self) -> list[int, int]:
        """Obtenir la liste des coordonnées des murs.

        Returns:
            list[int,int]: ...
        """
        return [[x, y] for y in range(self.height) for x in range(self.width) if self.case_est_mur(x, y)]

    def dessiner(self, generation=False):
        """Afficher le jeu sur les LEDs !"""

        # Pour un jeu avec LEDs, on affiche tous les objets dessus.

        if self.SUR_LEDS:
            if generation:

                for y in range(self.height):
                    for x in range(self.width):
                        case = self.grille[y][x]
                        if case.valeur == 1:
                            self.afficherSurLed(x, y, color="MUR")
                        else:
                            self.afficherSurLed(x, y)
            else:

                x,y = self.fin
                self.afficherSurLed(x, y, color="FIN")  # La case de fin.
                x, y = self.joueur.coords()
                # La case du personnage.
                self.afficherSurLed(x, y, color="PERSO")

            self.strand.show()

        else:
            # On créé un dictionnaire contenant toutes les coordonnées des objets.
            self.game_status = {
                "murs": self.get_murs(),
                'fin': [self.fin],
                "pos1": [self.joueur.coords()],
            }
            # Mettre à jour la simulation avec le dictionnaire.
            self.pseudo_leds.update(self.game_status)

    def initMoteur(self) -> None:
        """Initialisation du moteur, pour fair tourner le cylindre.
        """
        servoPIN = 18
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(servoPIN, GPIO.OUT)
        self.p = GPIO.PWM(servoPIN, 50)  # GPIO 17 for PWM with 50Hz
        self.p.start(0)  # Initialization

    def quitter(self):
        """Quitter le jeu"""
        if self.SUR_LEDS:
            eteint.eteint()
            self.manette.quit()
            pygame.quit()
            self.p.stop()
            GPIO.cleanup()
        else:
            self.listener.stop()

    def mainloop(self):
        """Boucle principale"""

        # Initialisation d'une variable de temps.
        temps_debut = time.time()

        # Tant que le jeu n'est pas fini.
        while self.jeu:

            # Temporisation
            time.sleep(0.1)

            # Si les LEDs sont branchées, on récupère les entrées manette.
            if self.SUR_LEDS:
                self.key_press_manette()

            # Déplacer le joueur dans la direction voulue.
            self.joueur.deplacement(self.voisins_joueur(), self.height)

            # Vérifier si le joueur a atteint la case finale.
            self.joueur_atteint_fin()

            # Dessiner le jeu.
            self.dessiner()

        # Une fois le jeu fini, fermer les processus appelés.
        self.quitter()

        # Renvoyé un score calculé à partir du temps de résolution du joueur.
        temps = round(time.time()-temps_debut, 3)
        return print(round((1/temps)*1000))


if __name__ == "__main__":
    laby = Labyrinthe()
    laby.mainloop()
