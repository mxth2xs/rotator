
import importlib, rotator.algo.config, PIL.ImageGrab, tkinter.colorchooser, os, random
import tkinter as tk
for lib in rotator.algo.config.modnames:
    globals()[lib] = importlib.import_module(lib)

from rotator.algo.image_maker import image_maker 

class SCRIBBLE:

    """"""
    
    def __init__(self) -> None:
        try:
            from rotator.algo.config import height_screen, width_screen, WIDTH, HEIGHT
            self.height_screen, self.width_screen, self.width, self.height = height_screen, width_screen, WIDTH, HEIGHT
        except:
            self.height_screen, self.width_screen, self.height, self.width = 1080, 1920, 44, 22
        
        self.taille_grille = 20
        self.image = False

        #Lancement de la fenêtre Canvas
        self.scribble = tk.Tk()
        self.C = tk.Canvas(self.scribble, bg="black", width=self.width*self.taille_grille, height=self.height*self.taille_grille)
        
        self.colors = ((0, 0, 0), "#000000")


        self.creationListeCouleurs()
        tk.Button(self.scribble, text='Choisir une couleur',                command=self.changerCouleur).grid(row=0, column=0)
        tk.Button(self.scribble, text='Exporter image_pixel',               command=self.export_image)  .grid(row = 0, column=1)
        tk.Button(self.scribble, text='Afficher image_pixel sur les leds',  command=self.print_image)   .grid(row = 0, column=2)
        tk.Button(self.scribble, text='Supprimer dessin',                   command=self.destroy)       .grid(row = 0, column=3)

    def creationListeCouleurs(self):
        """Création de la liste de couleur vide + création de la grille de fond

        Returns:
            list_color (list): Liste des couleurs de chaque pixel du dessin.
        """
        self.list_color = []
    
        for y in range(self.height):
            self.list_color.append([])
            for x in range(self.width):
                self.list_color[y].append(["x", "r", "g", "b", self.C.create_rectangle(x*self.taille_grille, y*self.taille_grille, x*self.taille_grille+self.taille_grille, y*self.taille_grille+self.taille_grille, fill = "#000000", outline="#FFFF00")])
        self.C.grid(row=1, column=0, columnspan=4)
                
        return self.list_color

    def changerCouleur(self):
        """Bouton pour changer de couleur.
        """
        self.colors = tkinter.colorchooser.askcolor(title="Tkinter Color Chooser")
        self.scribble.configure(bg=self.colors[1])

    def export_image(self):
        """Bouton pour exporter l'image.
        """
        x = self.C.winfo_rootx()
        y = self.C.winfo_rooty()
        w = self.C.winfo_width()
        h = self.C.winfo_height()
        self.img= PIL.ImageGrab.grab((x, y, x+w, y+h)).save("rotator/images/scribble_creation/IMAGE-"+ str(len(os.listdir('rotator/images/scribble_creation'))+1) +".png")
        self.image = True
    
    def print_image(self):
        """Bouton pour afficher l'image (utilisation du module image_maker).
        """
        if self.image == True:
            image_maker("scribble_creation", self.list_color)
            self.image = False
        else:
            x = self.C.winfo_rootx()
            y = self.C.winfo_rooty()
            w = self.C.winfo_width()
            h = self.C.winfo_height()
            self.img= PIL.ImageGrab.grab((x, y, x+w, y+h)).save("rotator/images/scribble_creation/IMAGE-"+ str(len(os.listdir('rotator/images/scribble_creation')+1) +".jpg"))
            image_maker("scribble_creation", self.list_color)
            os.remove("images/scribble_creation/IMAGE-"+ str(len(os.listdir(os.listdir('rotator/images/scribble_creation'))+1) +".jpg"))

    def destroy(self):
        """Recommencer le dessin.
        """
        self.C.delete('all')
        self.creationListeCouleurs()

    def click(self, event, couleur):
        self.X = event.x
        self.Y = event.y
        dedans_haut = self.X >= 0
        dedans_gauche = self.Y >= 0
        dedans_bas = self.X < self.width*self.taille_grille
        dedans_droite = self.Y < self.height*self.taille_grille
        
        if dedans_haut and dedans_gauche and dedans_bas and dedans_droite:
            self.C.itemconfig(self.list_color[self.Y//self.taille_grille][self.X//self.taille_grille][4], fill = couleur)

            if self.Y//self.taille_grille%2 == 0:
                led = self.list_color[self.Y//self.taille_grille][self.X//self.taille_grille][4]-1

            else:
                led = (self.Y//self.taille_grille+1)*(self.width-1)+self.Y//self.taille_grille-self.X//self.taille_grille

            self.list_color[self.Y//self.taille_grille][self.X//self.taille_grille][0] = led
            self.list_color[self.Y//self.taille_grille][self.X//self.taille_grille][1] = self.colors[0][0]
            self.list_color[self.Y//self.taille_grille][self.X//self.taille_grille][2] = self.colors[0][1]
            self.list_color[self.Y//self.taille_grille][self.X//self.taille_grille][3] = self.colors[0][2]


def scribble():
    game = SCRIBBLE()
    game.scribble.bind('<B1-Motion>',lambda event: game.click(event, game.colors[1]))
    game.scribble.bind('<Button-1>',lambda event: game.click(event, game.colors[1]))
    game.scribble.bind('<B2-Motion>',lambda event: game.click(event, "#"+"".join(random.sample(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'], 6))))
    game.scribble.bind('<Button-2>',lambda event: game.click(event, "#"+"".join(random.sample(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'], 6))))
    game.scribble.bind('<B3-Motion>',lambda event: game.click(event, '#000000'))
    game.scribble.bind('<Button-3>',lambda event: game.click(event, '#000000'))
    
    
    game.scribble.mainloop()
    

if __name__ == "__main__":
    scribble()